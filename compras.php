<?php
	$consulta="SELECT idproveedor, nombre, rfc FROM proveedor ORDER BY nombre ASC";
	$resultado=bd_consulta($consulta);
	$consultaPro="SELECT idproducto, nombre, stock, precio FROM producto ORDER BY nombre ASC";
	$resultadoPro=bd_consulta($consultaPro);
?>

<section id="form_box">
			<header id="header_form"> Agregar una factura</header>
			<form id="miformulario" action="compras_procesa.php" method="get" enctype="application/x-www-form-urlencoded">
				<div class="myField">				
					<label for="proveedor"> Proveedor: </label>
					<select class="extralargo" name="proveedor" id="proveedor" onchange="actualizaRfc();">
					<?php
					echo '<option mi_rfc="0" value="-1"></option>';
					while($row=mysqli_fetch_assoc($resultado)){
						echo '<option mi_rfc="'.$row["rfc"].'" value="'.$row["idproveedor"].' ">';
						//echo '<option value="'.$row["idproveedor"].'">';
						echo $row["nombre"];
						echo '</option>';
					}
					?>
					</select>
				</div>	
				<div class="myField">
					<label for="fecha"> Fecha: </label>
					<input type="date" class="corto" name="fecha" id="fecha"  placeholder="Fecha" required> 
				</div>	
				<div class="myField">
					<label for="folio"> Folio: </label>
					<input type="number" class="extracorto" name="folio" id="folio"  min="0" 
																						required> 
				</div>	
				<div class="myField">				
					<label for="rfc"> RFC: </label>
					<input class="corto" name="rfc" id="rfc" readonly> 							  			  
				</div>
				<div class="myField">
					<label for="fecha2"> Fecha Server: </label>
					<input type="date" class="corto" name="fecha2" id="fecha2"  placeholder="Fecha" value="<?php echo date("Y-m-d"); ?>"> 
				</div>
				<table id="tabla">
					<thead id="encabezado_tabla">
						<tr>
							<td>#</td>
							<td>Cant</td>
							<td >Artículo/Servicio</td>
							<td>Precio Unitario</td>
							<td>SubTotal</td>								
						</tr>
					</thead>
					
					<?php for($i=1;$i<=8;$i++){ ?>
					<tr>
						<td> <input type="text" value="0<?php echo $i; ?>" readonly /> </td>
						<td> <input type="number" min="1"  value="1" name="cantidad<?php echo $i; ?>"id="cantidad<?php echo $i; ?>" onchange="getSubtotal(<?php echo $i; ?>);" />	</td>   
						<td> <select class="extralargo" name="producto<?php echo $i ?>" id="producto<?php echo $i ?>" onchange="actualizaPrecio(<?php  echo $i ?>);">
					<?php
					echo '<option mi_precio="0" value="-1"></option>';
					mysqli_data_seek($resultadoPro, 0);
					while($row=mysqli_fetch_assoc($resultadoPro)){
						echo '<option mi_precio="'.$row["precio"].'" value="'.$row["idproducto"].' ">';
						//echo '<option value="'.$row["idproducto"].'">';
						echo $row["nombre"];
						echo '</option>';
					}
					?>
					</select> </td>
						
						
						<td> <input type="number" min="0" value="0.00"  id="precio<?php echo $i; ?>"name="precio<?php echo $i; ?>" onchange="getSubtotal(<?php echo $i; ?>);" /> </td>
						
						<td> <input type="text"  readonly value="0.00"  id="subtotal<?php echo $i; ?>" /> </td>					
					</tr>
					<?php } ?>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td> <input type="text" value="Subtotal" readonly class="totales" /> </td>
						<td> <input type="text" readonly value="0.00" id="subtotal"></td>
						
						
					</tr>
					
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td> <input type="text" value="Impuesto" readonly class="totales" /> </td>
						<td> <input type="text" readonly value="0.00" id="impuesto"></td>
						
					</tr>
					
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td> <input type="text" value="Total" readonly class="totales" /> </td>
						<td> <input type="text" readonly value="0.00" id="total"></td>
						
					</tr>
				</table>
				<div class="myField">	
				<input form="miformulario" type="submit" class="formButton" value="Enviar" autofocus> 
				<input type="reset" class="formButton" name="Cancelar" value="Cancelar" >			
			</div>
			</form>
		</section>