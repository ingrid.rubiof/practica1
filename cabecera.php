<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Formulario con HTML5">
		<meta name="keywords" content="HTML5, CSS3, Javascript">
		<title>Formulario de Compra</title>
		<link rel="stylesheet" href="mis_estilos.css">		
		<script src="mis_estilos.js"></script>
		</head>
		<body> 
			<div id="documento">
				<header id="cabecera">
					<h1>Formulario de Compra usando HTML 5</h1>
				</header>
				<nav id="menu_box">  
					<ul>
						<li><a href="index.php?op=10">Facturas</a></li>
						<li><a href="index.php?op=20">Proveedores</a></li>
						<li><a href="index.php?op=30">Productos</a></li>
						<li><a href="index.php?op=40">Usuarios</a></li>
						<li><a href="index.php?op=0">Salir</a></li>
					</ul>
				</nav> 