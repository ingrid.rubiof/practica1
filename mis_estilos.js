function getTotal(){
	//tabla=document.getElementById('tabla');
	//alert(tabla.row);
	//for(var i=1; i<tabla.row)
			var resultado=0;
			for(i=1;i<=8;i++){
				resultado=resultado+parseFloat(document.getElementById('subtotal'+i).value);
			}
			document.getElementById('subtotal').value=resultado;
			
			var imp=resultado*.16;
			document.getElementById('impuesto').value=imp;
			
			document.getElementById('total').value=(resultado+imp)
		}
		
		
		
		function number_format(amount, decimals) {

			amount += ''; // por si pasan un numero en vez de un string
			amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

			decimals = decimals || 0; // por si la variable no fue fue pasada

			// si no es un numero o es igual a cero retorno el mismo cero
			if (isNaN(amount) || amount === 0) 
				return parseFloat(0).toFixed(decimals);

			// si es mayor o menor que cero retorno el valor formateado como numero
			amount = '' + amount.toFixed(decimals);

			var amount_parts = amount.split('.'),
				regexp = /(\d+)(\d{10})/;

			while (regexp.test(amount_parts[0]))
				amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
			//alert regexp;
			return amount_parts.join('.');
		}		
				
		function getSubtotal(i)
		{

			cantidad=document.getElementById('cantidad'+i).value;	
			precio=document.getElementById('precio'+i).value;
			//document.getElementById('subtotal'+i).setAttribute("value",number_format(precio*cantidad,2));	
			document.getElementById('subtotal'+i).value=number_format(precio*cantidad,2);
			
			getTotal();
		}
		
		function inicializa_fecha(){
			var f = new Date();
			var mes = f.getMonth()+1;
			var dia = f.getDate();
			var ano = f.getFullYear();
			if(dia<10)
				dia='0'+dia;
			if(mes<10)
				mes='0'+mes;
			
			document.getElementById('fecha').value=ano+"-"+mes+"-"+dia;
			//document.getElementById('fecha').setAttribute("value", ano+"-"+mes+"-"+dia);
		}
		function actualizaRfc(){
			mySelect=document.getElementById('proveedor');
			mi_nuevo_rfc=mySelect.options[mySelect.selectedIndex].getAttribute("mi_rfc");
			document.getElementById('rfc').value=mi_nuevo_rfc;
		}
		function actualizaPrecio(i){
			mySelect=document.getElementById('producto'+i);
			mi_nuevo_precio=mySelect.options[mySelect.selectedIndex].getAttribute("mi_precio");
			document.getElementById('precio'+i).value=mi_nuevo_precio;
			getSubtotal(i);
		}
		window.onload=inicializa_fecha;